
*-*-*-* SERVICIO DE CONSULTA *-*-*-*

Tecnologias a utilizar.

python == 3.9
Django==3.2.6
django-filter==2.4.0
djangorestframework==3.12.4
mysql==5.7.12
pytest==6.2.4
pytest-django==4.4.0
pytest-factoryboy==2.1.0


Para probar se tiene que agregar la variable de entorno desde la terminal:

set DJANGO_SETTINGS_MODULE=properties.settings

1.Comenzare por al creación del proyecto y despues la creación de la app.
2.Conectare mi aplicación a la base de datos de habi.
3.Ya que la base de datos ya existe usare la siguente linea para crear los modelos:

    python3 manage.py inspectbb > models.py

4.Analizar los campos de los modelos y ver sus nombres para poder hacer uso de ellos.
5.Creare el selializador y el viewset.
6.Hare pruebas para saber si se estan jalando los datos correctamente.
7. Creare los filtros utilizando DjangoFilterBackend incluido en django_filters


*-*-*-* SERVICIO DE "ME GUSTA" *-*-*-*

Agrege una tabla que guardara el id de la propiedad, el usuario que esta logeado en ese momento y un campo boleano en
donde se guardara si le dio like o dislike.

Pense en no poner este boleano ya que solo se require que en los requerimientos solo habla de darle like a la propiedad,
más no dislike, pero normalmente esto sea hace, ya que puede que al usuario siempre no el guste la propiedad y ya no la
quiera ver en su listado de likes.

Otra opcion que pense es simplemente ligar la propiedad con el usuario sin necesidad del booleano ya que no se habla
de la opción de dislike, pero si esta existiera en el front, esto podria arreglarse simplemente borrando el registro que
une al usuario con esa propiedad, estoy ayudaria a que no hubiera tantos registros en la base de datos, que quiza no sean
necesarios para la empresa.

Utilize el unique_together para el usuario y la propiedad, ya que estoy evitara que si el usuario da click de nuevo
en la propiedad se cree otro registro, causando que haya muchos registros repetidos en la base de datos.


migrations.CreateModel(
            name='PropertyLikes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('like', models.BooleanField(default=False)),
                ('property', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='properties.property')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('user', 'property')},
            },
        ),

CREATE TABLE `propertylikes` (`id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY, `like` bool NOT NULL, `property_id` integer NOT NULL, `user_id` integer NOT NULL);
ALTER TABLE `propertylikes` ADD CONSTRAINT `propertylikes_user_id_property_id_7bccd59e_uniq` UNIQUE (`user_id`, `property_id`);
ALTER TABLE `propertylikes` ADD CONSTRAINT `propertylikes_property_id_8fb00ad4_fk_property_id` FOREIGN KEY (`property_id`) REFERENCES `property` (`id`);
ALTER TABLE `propertylikes` ADD CONSTRAINT `propertylikes_user_id_2a4c5d6d_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
